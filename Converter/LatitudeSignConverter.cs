﻿using System;
using System.Windows;
using System.Windows.Data;

namespace UIMap
{
    public class LatitudeSignConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //if (value == null || !(value is bool))
            //    return Binding.DoNothing;

            return (double)value >= 0 ? "N" : "S";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}

