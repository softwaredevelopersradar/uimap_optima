﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VJammer
    {
        public int ID { get; private set; }
        public IMapObject mapJammer { get; set; }

        public Feature mapZoneBearing { get; set; }
        public Feature mapZoneJamming { get; set; }

        public Feature mapZoneJammingSecond { get; set; }

        public Feature mapZoneConnectLink { get; set; }

        public Feature mapZoneConnectPC { get; set; }

        public Feature mapZoneOptic { get; set; }
        public Feature mapZoneAttention { get; set; }
        public Feature mapZoneReadiness { get; set; }
        public Feature mapZoneAlarm { get; set; }

        public Feature mapZoneRotate { get; set; }


        public Feature mapZoneRotateJamming { get; set; }
        public Feature mapZoneRotateJammingSecond { get; set; }
        public Feature mapZoneRotateLink { get; set; }
        public Feature mapZoneRotatePC { get; set; }


        public List<Feature> mapBearing { get; set; }

        public VJammer (int Id)
        {
            ID = Id;
            mapZoneBearing = new Feature();
            mapZoneJamming = new Feature();
            mapZoneJammingSecond = new Feature();
            mapZoneConnectLink = new Feature();
            mapZoneConnectPC = new Feature();
            mapZoneOptic = new Feature();
            mapZoneAttention = new Feature();
            mapZoneReadiness = new Feature();
            mapZoneAlarm = new Feature();
            mapZoneRotate = new Feature();

            mapZoneRotateJamming = new Feature();
            mapZoneRotateJammingSecond = new Feature();
            mapZoneRotateLink = new Feature();
            mapZoneRotatePC = new Feature();

            mapBearing = new List<Feature>();
        }


    }
}
