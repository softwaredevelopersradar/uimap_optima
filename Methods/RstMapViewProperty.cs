﻿
using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Mapsui.Geometries;
using UIMap.Models;
using WpfMapControl;

namespace UIMap
{
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {

        #region Property

        private bool _visibleZoneBearing = true;
        public bool VisibleZoneBearing
        {
            get { return _visibleZoneBearing; }
            set
            {
                if (_visibleZoneBearing != value)
                {
                    _visibleZoneBearing = value;
                    OnPropertyChanged();

                    try
                    {
                        if(Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneBearing.Visible = VisibleZoneBearing;
                    }
                    catch
                    { }

                }
            }
        }



        private bool _visibleZoneJamming = true;
        public bool VisibleZoneJamming
        {
            get { return _visibleZoneJamming; }
            set
            {
                if (_visibleZoneJamming != value)
                {
                    _visibleZoneJamming = value;
                    OnPropertyChanged();

                    try
                    {
                        if (Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJamming.Visible = VisibleZoneJamming;

                    }
                    catch
                    { }

                }
            }
        }


        private bool _visibleZoneJammingSecond = true;
        public bool VisibleZoneJammingSecond
        {
            get { return _visibleZoneJammingSecond; }
            set
            {
                if (_visibleZoneJammingSecond != value)
                {
                    _visibleZoneJammingSecond = value;
                    OnPropertyChanged();

                    try
                    {
                        if (Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJammingSecond.Visible = VisibleZoneJammingSecond;

                    }
                    catch
                    { }

                }
            }
        }


        private bool _visibleZoneConnectLink = true;
        public bool VisibleZoneConnectLink
        {
            get { return _visibleZoneConnectLink; }
            set
            {
                if (_visibleZoneConnectLink != value)
                {
                    _visibleZoneConnectLink = value;
                    OnPropertyChanged();

                    try
                    {
                        if (Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnectLink.Visible = VisibleZoneConnectLink;

                    }
                    catch
                    { }

                }
            }
        }


        private bool _visibleZoneConnectPC = true;
        public bool VisibleZoneConnectPC
        {
            get { return _visibleZoneConnectPC; }
            set
            {
                if (_visibleZoneConnectPC != value)
                {
                    _visibleZoneConnectPC = value;
                    OnPropertyChanged();

                    try
                    {
                        if (Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnectPC.Visible = VisibleZoneConnectPC;

                    }
                    catch
                    { }

                }
            }
        }

        

        private bool _visibleZoneOptic = true;
        public bool VisibleZoneOptic
        {
            get { return _visibleZoneOptic; }
            set
            {
                if (_visibleZoneOptic != value)
                {
                    _visibleZoneOptic = value;
                    OnPropertyChanged();

                    try
                    {
                        if (Jammers.Count != 0)
                            Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Visible = VisibleZoneOptic;
                    }
                    catch
                    { }
                }
            }
        }





        private bool _visibleZoneCircle = false;

        public bool VisibleZoneCircle
        {
            get { return _visibleZoneCircle; }
            set
            {
                if (_visibleZoneCircle != value)
                {
                    _visibleZoneCircle = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var jammer in Jammers)
                        {
                            jammer.ZoneAlarm.Visible = VisibleZoneCircle;
                            jammer.ZoneAttention.Visible = VisibleZoneCircle;
                            jammer.ZoneReadiness.Visible = VisibleZoneCircle;
                        }
                    }
                    catch
                    { }
                }
            }
        }

        private bool _visibleBearing = true;

        public bool VisibleBearing
        {
            get { return _visibleBearing; }
            set
            {
                if (_visibleBearing != value)
                {
                    _visibleBearing = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var jammer in Jammers)
                        {
                            foreach (var bearing in jammer.BearingSource)
                                bearing.Visible = VisibleBearing;

                            Jammer_OnUpdateBearing(jammer, new EventArgs());
                        }
                    }
                    catch
                    { }
                }
            }
        }

        private bool _visibleDrone = true;

        public bool VisibleDrone
        {
            get { return _visibleDrone; }
            set
            {
                if (_visibleDrone != value)
                {
                    _visibleDrone = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var drone in Drones)
                            drone.Visible = VisibleDrone;

                        UpdateDrones();
                    }
                    catch
                    { }
                }
            }
        }

        private bool _visibleTrackDrone = false;

        public bool VisibleTrackDrone
        {
            get { return _visibleTrackDrone; }
            set
            {
                if (_visibleTrackDrone != value)
                {
                    _visibleTrackDrone = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var drone in Drones)
                            drone.VisibleTrack = VisibleTrackDrone;

                        UpdateDrones();
                    }
                    catch
                    { }
                }
            }
        }


        private bool _visibleTrackRodnik = true;

        public bool VisibleTrackRodnik
        {
            get { return _visibleTrackRodnik; }
            set
            {
                if (_visibleTrackRodnik != value)
                {
                    _visibleTrackRodnik = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var rodnik in RodnikTargets)
                            rodnik.VisibleTrack = VisibleTrackRodnik;

                        UpdateRodnikTargets();
                    }
                    catch
                    { }
                }
            }
        }



        private bool _visibleTrackZorkiR = true;

        public bool VisibleTrackZorkiR
        {
            get { return _visibleTrackZorkiR; }
            set
            {
                if (_visibleTrackZorkiR != value)
                {
                    _visibleTrackZorkiR = value;
                    OnPropertyChanged();

                    try
                    {
                        foreach (var rodnik in ZorkiRTargets)
                            rodnik.VisibleTrack = _visibleTrackZorkiR;

                        UpdateZorkiRTargets();
                    }
                    catch
                    { }
                }
            }
        }
        





        private bool _visibleGNSSPosition = false;

        public bool VisibleGNSSPosition
        {
            get { return _visibleGNSSPosition; }
            set
            {
                if (_visibleGNSSPosition != value)
                {
                    _visibleGNSSPosition = value;
                    OnPropertyChanged();

                    try
                    {
                        UpdateGNSS();
                    }
                    catch
                    { }
                }
            }
        }

        private bool _visibleSpoofingPosition = false;

        public bool VisibleSpoofingPosition
        {
            get { return _visibleSpoofingPosition; }
            set
            {
                if (_visibleSpoofingPosition != value)
                {
                    _visibleSpoofingPosition = value;
                    OnPropertyChanged();

                    try
                    {
                        UpdateSpoofing();
                    }
                    catch
                    { }
                }
            }
        }



        #region CuirasseTargetVisible

        private bool _visibleCuirasseTarget = true;

        public bool VisibleCuirasseTarget
        {
            get { return _visibleCuirasseTarget; }
            set
            {
                if (_visibleCuirasseTarget != value)
                {
                    _visibleCuirasseTarget = value;
                    OnPropertyChanged();

                    try
                    {
                        UpdateCuirasseTargets();
                    }
                    catch
                    { }
                }
            }
        }

       
        #endregion


        public void NavigateTo(Location coord)
        {
            mapControl.NavigateTo(new Mapsui.Geometries.Point(coord.Longitude, coord.Latitude));
        }


        private Location _coordinate;

        public Location Coordinate
        {
            get => _coordinate;
            set
            {

                _coordinate = value;

                LocationPos = new LocationFormat(Coordinate.Latitude, Coordinate.Longitude, FormatViewCoord);

     
                OnPropertyChanged();
            }
        }


        private LocationFormat _locationPos ;

        public LocationFormat LocationPos
        {
            get => _locationPos;
            set
            {

                _locationPos = value;

                OnPropertyChanged();
            }
        }
        

        
        


    private Location _startPointRout = new Location(-1, -1);

        public Location StartPointRout
        {
            get => _startPointRout;
            set
            {
                _startPointRout = value;
                UpdateStartPosition();
                DrawRout();
                OnPropertyChanged();
            }
        }

        private Location _stopPointRout = new Location(-1,-1);

        public Location StopPointRout
        {
            get => _stopPointRout;
            set
            {
                _stopPointRout = value;
                UpdateStopPosition(0);
                DrawRout();
                OnPropertyChanged();
            }
        }

        private Location _clickCoord;

        public Location ClickCoord
        {
            get => _clickCoord;
            set
            {
                _clickCoord = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<VMJammer> _jammers = new ObservableCollection<VMJammer>();
        public ObservableCollection<VMJammer> Jammers
        {
            get => _jammers;
            set
            {
                if (_jammers != null && _jammers.Equals(value)) return;

                _jammers = value;
                UpdateJammers();

            }
        }

        private ObservableCollection<VMDrone> _drones = new ObservableCollection<VMDrone>();
        public ObservableCollection<VMDrone> Drones
        {
            get => _drones;
            set
            {
                if (_drones != null && _drones.Equals(value)) return;
                _drones = value;

                foreach (var d in _drones)
                {
                    d.Visible = VisibleDrone;
                    d.VisibleTrack = VisibleTrackDrone;
                }

                UpdateDrones();

            }
        }



        private ObservableCollection<VMRodnikTarget> _rodnikTargets = new ObservableCollection<VMRodnikTarget>();
        public ObservableCollection<VMRodnikTarget> RodnikTargets
        {
            get => _rodnikTargets;
            set
            {
                if (_rodnikTargets != null && _rodnikTargets.Equals(value)) return;
                _rodnikTargets = value;

                foreach (var d in _rodnikTargets)
                    d.VisibleTrack = VisibleTrackRodnik;

                UpdateRodnikTargets();

            }
        }


        private ObservableCollection<VMZorkiRTarget> _zorkiRTargets = new ObservableCollection<VMZorkiRTarget>();
        public ObservableCollection<VMZorkiRTarget> ZorkiRTargets
        {
            get => _zorkiRTargets;
            set
            {
                if (_zorkiRTargets != null && _zorkiRTargets.Equals(value)) return;
                _zorkiRTargets = value;

                foreach (var d in _zorkiRTargets)
                    d.VisibleTrack = VisibleTrackZorkiR;

                UpdateZorkiRTargets();

            }
        }
        #endregion


        private ObservableCollection<VMCuirasseTarget> _cuirasseTargets = new ObservableCollection<VMCuirasseTarget>();
        public ObservableCollection<VMCuirasseTarget> CuirasseTargets
        {
            get => _cuirasseTargets;
            set
            {
                if (_cuirasseTargets != null && _cuirasseTargets.Equals(value)) return;
                _cuirasseTargets = value;
                
                UpdateCuirasseTargets();

            }
        }


        private byte _zone1TDF;

        public byte Zone1TDF
        {
            get => _zone1TDF;
            set
            {
                _zone1TDF = value;
                OnPropertyChanged();
            }
        }

        private byte _zone2TDF;

        public byte Zone2TDF
        {
            get => _zone2TDF;
            set
            {
                _zone2TDF = value;
                OnPropertyChanged();
            }
        }

        private byte _zone3TDF;

        public byte Zone3TDF
        {
            get => _zone3TDF;
            set
            {
                _zone3TDF = value;
                OnPropertyChanged();
            }
        }

        private double _currentScale;
        public double CurrentScale
        {
            get => _currentScale;
            set
            {
                _currentScale = value;

                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

    }
}