﻿using GrozaSModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap
{
    public class VMRodnikTarget : INotifyPropertyChanged
    {
        Location locNull = new Location(-1, -1);

        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {                
                _locationTrack = value;
                OnPropertyChanged();
            }
        }


        private int _idSource;

        public int IDSource
        {
            get => _idSource;
            set
            {
                if (_idSource.Equals(value)) return;
                _idSource = value;

            }
        }

        private string _name = "";

        public string Name
        {
            get => _name;
            set
            {
                if (_name.Equals(value)) return;
                _name = value;

            }
        }


        private bool _visible = true;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                OnPropertyChanged();

            }
        }

        private ViewRodnik _view;

        public ViewRodnik View
        {
            get => _view;
            set
            {
                if (_view.Equals(value)) return;
                _view = value;
                OnPropertyChanged();

            }
        }


        private bool _visibleTrack = false;

        public bool VisibleTrack
        {
            get => _visibleTrack;
            set
            {
                if (_visibleTrack.Equals(value)) return;
                _visibleTrack = value;
                OnPropertyChanged();

            }
        }

        public Mapsui.Styles.Pen PenTrack { get; set; }
   



        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }


        private float _azimuth = -1;

        public float Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;

              
            }
        }


        private float _range = 0;

        public float Range
        {
            get { return _range; }
            set
            {
                if (_range.Equals(value)) return;
                _range = value;

            }
        }

        private float _velocity;

        public float Velocity
        {
            get { return _velocity; }
            set
            {
                if (_velocity.Equals(value)) return;
                _velocity = value;

            }
        }


        private float _aspect;

        public float Aspect
        {
            get { return _aspect; }
            set
            {
                if (_aspect.Equals(value)) return;
                _aspect = value;

            }
        }


        public VMRodnikTarget() 
        {
            PenTrack =  new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
