﻿using GrozaSModelsDBLib;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Mapsui.Geometries;
using UIMap.Models;
using WpfMapControl;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UIMap
{
    /// <summary>
    /// Interaction logic for RstMapView.xaml
    /// </summary>
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {

        public event EventHandler<int> MenuItemStationClick;
        public event EventHandler<string> MenuItemMainStationClick;
        public event EventHandler<RoutedEventArgs> MenuItemDeleteAllStationClick;
        public event EventHandler<Location> OnCoordinateForSaveUpdate;

        System.Timers.Timer timer = new System.Timers.Timer();

        private bool DragMouse = false;
        Location locationZero = new Location(0, 0);

        public RstMapView()
        {
            InitializeComponent();

            DataContext = this;

            InitializeObjectStyles();

            InitializeRodnikObjectStyles();

            InitializeZorkiRObjectStyles();

            InitializeCuirasseObjectStyles();


            mapControl.MapMouseMoveEvent += OnMapMouseMove;
            mapControl.ResolutionChangedEvent += OnResolutionCahnged;
            mapControl.MapClickEvent += OnMapClick;
            mapControl.MouseLeftButtonDown += MapControl_MouseLeftButtonDown;
            mapControl.MouseLeftButtonUp += MapControl_MouseLeftButtonUp;
            mapControl.MouseMove += MapControl_MouseMove;
            



            

            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 300;
            //timer.Start();


            Coordinate = new Location();

            LocationPos = new LocationFormat();

            FormatViewCoord = FormatCoord.DD_MM_mm;


            Projection = MapProjection.Geo;

            Drones.CollectionChanged += Drones_CollectionChanged;
            Jammers.CollectionChanged += Jammers_CollectionChanged;


            SetResourceLanguage(MapLanguages.EN); 


            InitZorkiRTargetsVisible();



        }

        #region Dependency property

        private static readonly DependencyProperty dependencyVisibilityDirectionContextMenuIte =
         DependencyProperty.Register("VisibilityDirectionContextMenuItem", typeof(Visibility), typeof(RstMapView), new PropertyMetadata(Visibility.Visible, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        private static readonly DependencyProperty dependencyVisibilityASYContextMenuIte =
    DependencyProperty.Register("VisibilityASYContextMenuItem", typeof(Visibility), typeof(RstMapView), new PropertyMetadata(Visibility.Visible, new PropertyChangedCallback(OnDependencyPropertyChanged)));

        public Visibility VisibilityDirectionContextMenuItem
        {
            get => (Visibility)GetValue(dependencyVisibilityDirectionContextMenuIte);
            set => SetValue(dependencyVisibilityDirectionContextMenuIte, value);
        }

        public Visibility VisibilityASYContextMenuItem
        {
            get => (Visibility)GetValue(dependencyVisibilityASYContextMenuIte);
            set => SetValue(dependencyVisibilityASYContextMenuIte, value);
        }

        private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView mapObjectPropGrid = d as RstMapView;
            mapObjectPropGrid.OnDependencyPropertyChanged(e);
        }

        private void OnDependencyPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(VisibilityDirectionContextMenuItem):
                    VisibilityDirectionContextMenuItem_ = (Visibility)e.NewValue;
                    break;
                case nameof(VisibilityASYContextMenuItem):
                    VisibilityASYContextMenuItem_ = (Visibility)e.NewValue;
                    break;

            }
        }

        private Visibility visibilityDirectionContextMenuItem_;

        public Visibility VisibilityDirectionContextMenuItem_
        {
            get => visibilityDirectionContextMenuItem_;
            set
            {
                if (visibilityDirectionContextMenuItem_ == value) return;
                visibilityDirectionContextMenuItem_ = value;
                OnPropertyChanged();
            }
        }


        private Visibility visibilityASYContextMenuItem_;

        public Visibility VisibilityASYContextMenuItem_
        {
            get => visibilityASYContextMenuItem_;
            set
            {
                if (visibilityASYContextMenuItem_ == value) return;
                visibilityASYContextMenuItem_ = value;
                OnPropertyChanged();
            }
        }

        #endregion



        private void SetDirectionMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            
            

            OnDirection?.Invoke(this, GetBeraingClickCoord());
        }



        private void InitZorkiRTargetsVisible()
        {
            ZorkiRTargetVisible.Add(ViewZorkiR.Car, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Group, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Helicopter, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Human, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.IFV, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Tank, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.UAV, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Unknown, true);
        }


        private void Jammers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateJammers();
        } 

        private void Drones_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateDrones();
        }

        private void MapControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DragMouse = false;
        }

        private void MapControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMouse = true;
        }

        private void MapControl_MouseMove(object sender, MouseEventArgs e)
        {
         //   if (DragMouse && !CenterCoord.Equals(locationZero))
           //     CenterCoord = locationZero;
        }

        private void OnMapMouseMove(object sender, Location location)
        {
            Coordinate = location;
        }

        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {
            if (e.ClickedButton.ChangedButton == MouseButton.Right)
            {

                ClickCoord = e.Location;
            }
        }

        private void OnResolutionCahnged(object sender, double resolution)
        {
            try
            {
                SliderZoom.Value = SliderZoom.Maximum - resolution + SliderZoom.Minimum;

                CurrentScale = resolution;
            }
            catch { }
        }

        public void SetScale(double scale)
        {


            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (!mapControl.IsMapLoaded)
                        return;

                    mapControl.Resolution = scale;


                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                PathMap = openFileDialog.FileName;
                OpenMap();
            }
        }

        private void SliderZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            mapControl.Resolution = SliderZoom.Maximum - SliderZoom.Value + SliderZoom.Minimum;
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            PathMap = "";
            CloseMap();
        }


        private void SetSpoofMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            SpoofingPosition = ClickCoord;


        }

        private void SetGNSSMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            GNSSPosition = ClickCoord;
        }

        private void SetJammerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            OnJammerPosition?.Invoke(this, ClickCoord);



        }




        void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Random random = new Random();



            foreach (var jam in Jammers)
            {
                jam.ZoneBearing.Direction = random.Next(1, 359);
                jam.ZoneJamming.Direction = random.Next(1, 359);
                jam.ZoneOptic.Direction = random.Next(1, 359);
            }



            //timer.Stop();

        }

        private void SetDroneMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //if (!mapControl.IsMapLoaded)
            //{
            //    return;
            //}

            timer.Enabled = !timer.Enabled;
        }

        private void CenterButton_Click(object sender, RoutedEventArgs e)
        {
            CenterMapGNSS();
        }



        private void SetStartRoutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StartPointRout = ClickCoord;
        }

        private void SetStopRoutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StopPointRout = ClickCoord;
        }

        private void ClickRoutButton_Click(object sender, RoutedEventArgs e)
        {
            StartPointRout = new Location(-1, -1);
            StopPointRout = new Location(-1, -1);

        }

        private void OpenMatrixButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
           
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PathMatrix = dialog.SelectedPath;
                OpenMatrix();

            }


        }

        private float GetBeraingClickCoord()
        {
            if (!mapControl.IsMapLoaded)
            {
                return -1;
            }
            var DirectionPosition = ClickCoord;

            var OwnPosition = Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().Coordinates;

            return Azimuth.CalcAzimuth(OwnPosition.Latitude, OwnPosition.Longitude, ClickCoord.Latitude, ClickCoord.Longitude);
        }

        private void SetDirectionJammingMenuItem_Click(object sender, RoutedEventArgs e)
        {            
            OnDirectionJamming?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionJammingSecondMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnDirectionJammingSecond?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionConnectLinkMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnDirectionConnectLink?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionConnectPCMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnDirectionConnectPC?.Invoke(this, GetBeraingClickCoord());
        }


        public void ChangeNameOfMenuItemStation(int item, string name)
        {
            ((MenuItem)((MenuItem)mapControl.ContextMenu.Items.GetItemAt(1)).Items.GetItemAt(item)).Header += " - " + name;
        }

        public void ChangeIconOfMenuItemStation(int item, string path)
        {
            ((MenuItem)((MenuItem)mapControl.ContextMenu.Items.GetItemAt(1)).Items.GetItemAt(item)).Icon = new Image
            {
                Source = new BitmapImage(new Uri(path))
            };
        }

        public void ChangeIconOfAllControlStations(string path)
        {
            foreach (object subMenuItem in (mapControl.ContextMenu.Items.GetItemAt(0) as MenuItem).Items)
            {
                if (subMenuItem is MenuItem menu)
                {
                    menu.Icon = new Image
                    {
                        Source = new BitmapImage(new Uri(path))
                    };
                }
            }
        }

        public void ChangeIconOfMenuItemStation(string tag, string path)
        {
            foreach (object menuObject in mapControl.ContextMenu.Items)
            {
                if (menuObject is MenuItem menuItem)
                {
                    foreach (object subMenuObject in menuItem.Items)
                    {
                        if (subMenuObject is MenuItem subMenuItem)
                        {
                            if (subMenuItem.Tag.Equals(tag))
                            {
                                subMenuItem.Icon = new Image
                                {
                                    Source = new BitmapImage(new Uri(path))
                                };
                            }
                        }
                    }
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;

            MenuItemStationClick(sender, Convert.ToInt32(menuItem.Tag));
        }


        private void SetClearRoutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            mapControl.RemoveObject(_featureRout);
            StopPointRout = new Location(-1, -1);
            StartPointRout = new Location(-1, -1);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;

            MenuItemMainStationClick(sender, (string)menuItem.Tag);         
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            MenuItemDeleteAllStationClick(sender, e);
        }

        private void MenuItem_Click_CoordSave(object sender, RoutedEventArgs e)
        {
            OnCoordinateForSaveUpdate(sender, ClickCoord);
        }
    }
}
