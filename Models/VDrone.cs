﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VDrone
    {
        public IMapObject mapObject { get; set; }
        public Feature mapTrack { get; set; }


        private int _idSource;

        public int IDSource
        {
            get => _idSource;
            set
            {
                if (_idSource.Equals(value)) return;
                _idSource = value;

            }
        }

    }
}
