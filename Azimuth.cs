﻿using Mapsui.Projection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIMap
{
    public static class Azimuth
    {

        public static float CalcAzimuth(double Lat1, double Long1, double Lat2, double Long2)
        {
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double dX = 0;
            double dY = 0;
            double Beta = 0;
            double Beta_tmp = 0;
            // .......................................................................
            var p = Mercator.FromLonLat(Long1, Lat1);
            x1 = p.X;
            y1 = p.Y;

            var p1 = Mercator.FromLonLat(Long2, Lat2);
            x2 = p1.X;
            y2 = p1.Y;

            dX = x2 - x1;
            dY = y2 - y1;
            // .......................................................................

            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(Math.Abs(dX) / Math.Abs(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            //-------------------------------------------------------------------------

            return (float)Beta;

        }
    }

    
    // **************************************************************** CalculationAzimuth

}
