﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace UIMap
{
    

    public partial class RstMapView : UserControl
    {
        //public  string GetResourceTitle (string key)
        //{
        //    try
        //    {                
        //        //return (string)this.Resources[key];
        //    }

        //    catch
        //    {
        //        return "";
        //    }
            
        //}

        
        public void SetResourceLanguage(MapLanguages language)
        {

            this.Resources.MergedDictionaries.Clear();
         
            ResourceDictionary dictMapButtons = new ResourceDictionary();
            ResourceDictionary dictMapContextMenu = new ResourceDictionary();

            try
            {
                switch (language)
                {
                    case MapLanguages.EN:
                        

                        dictMapButtons.Source = new Uri("/UIMap;component/Languages/MapButtons/StringResource.EN.xaml",
                            UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/UIMap;component/Languages/MapContextMenu/StringResource.EN.xaml",
                            UriKind.Relative);
                        break;

                    case MapLanguages.RU:
                        dictMapButtons.Source = new Uri("/UIMap;component/Languages/MapButtons/StringResource.RU.xaml",
                            UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/UIMap;component/Languages/MapContextMenu/StringResource.RU.xaml",
                            UriKind.Relative);

                        break;

                    case MapLanguages.AZ:
                        dictMapButtons.Source = new Uri("/UIMap;component/Languages/MapButtons/StringResource.AZ.xaml",
                            UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/UIMap;component/Languages/MapContextMenu/StringResource.AZ.xaml",
                            UriKind.Relative);

                        break;

                    default:
                        dictMapButtons.Source = new Uri("/UIMap;component/Languages/MapButtons/StringResource.EN.xaml",
                            UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/UIMap;component/Languages/MapContextMenu/StringResource.EN.xaml",
                            UriKind.Relative);

                        break;
                }


                Application.Current.Resources.MergedDictionaries.Add(dictMapButtons);
                this.Resources.MergedDictionaries.Add(dictMapButtons);
                this.Resources.MergedDictionaries.Add(dictMapContextMenu);

            }
            catch (Exception ex)
            { }
        }

    }
}
