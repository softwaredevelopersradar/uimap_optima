﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIMap
{
    interface IRstMap
    {
        #region Properties

        string PathMap { get; set; }
        string PathMtr { get; set; }

        #endregion

        #region Methods

        bool OpenMap();
        bool CloseMap();

        void IncreaseScale();
        void DecreaseScale();

        #endregion


    }
}
