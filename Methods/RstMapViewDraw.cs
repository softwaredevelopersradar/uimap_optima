﻿using Mapsui.Projection;
using Mapsui.Providers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using WpfMapControl;

namespace UIMap
{
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {
        private IMapObject DrawObject(MapObjectStyle mapObjectStyle, Location location, string Caption)
        {
            try
            {
                return mapControl.AddMapObject(mapObjectStyle, Caption, ConvertToPoint(location));
            }
            catch
            {
                return null;
            }
        }

        private Feature DrawFeaturePolygon(TypeFeature Type, List<Location> location, Mapsui.Styles.Color fillColor, Mapsui.Styles.Pen outlinePen, double width)
        {
            Feature ifeature = new Feature();
            if (location == null)
                return ifeature;
            try
            {
                List<Mapsui.Geometries.Point> wgs84Points = new List<Mapsui.Geometries.Point>(location.Count);
                foreach (var l in location)
                    wgs84Points.Add(ConvertToPoint(new Location(l.Latitude, l.Longitude)));


                switch (Type)
                {
                    case TypeFeature.Polygon:
                        ifeature = mapControl.AddPolygon(wgs84Points, fillColor, outlinePen);
                        break;

                    case TypeFeature.Polyline:
                        ifeature = mapControl.AddPolyline(wgs84Points, outlinePen);
                        break;

                    default:
                        break;
                }
                return ifeature;

            }
            catch { return null; }


        }

        private Mapsui.Geometries.Point ConvertToPoint(Location location)
        {
            Mapsui.Geometries.Point wgs84Points = new Mapsui.Geometries.Point();

            try
            {
                switch ((MapProjection)Projection)
                {
                    case MapProjection.Geo:
                        wgs84Points = location.ToPoint();

                        break;

                    case MapProjection.Mercator:
                        wgs84Points = Mercator.FromLonLat(location.ToPoint().X, location.ToPoint().Y);
                        
                        break;

                    default:
                        break;
                }
            }
            catch { }

            return wgs84Points;
        }

    }
}