﻿public enum ViewZorkiR : byte
{
    Human = 0,
    Car = 1,
    Helicopter = 2,
    Group = 3,
    UAV = 4,
    Tank = 5,
    IFV = 6,
    Unknown = 7
}