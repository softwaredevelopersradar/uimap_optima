﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using UIMap.Models;
using WpfMapControl;

namespace UIMap
{
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {
        
        
        #region PathMap
        public static readonly DependencyProperty PathMapProperty = DependencyProperty.Register("PathMap", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathMapChanged)));

        public string PathMap
        {
            get { return (string)GetValue(PathMapProperty); }
            set { SetValue(PathMapProperty, value); }
        }

        private static void PathMapChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.OpenMap();
            }
            catch
            { }

        }
                
        #endregion

        #region PathMapTile
        public static readonly DependencyProperty PathMapTileProperty = DependencyProperty.Register("PathMapTile", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathMapTileChanged)));

        public string PathMapTile
        {
            get { return (string)GetValue(PathMapTileProperty); }
            set { SetValue(PathMapTileProperty, value); }
        }

        private static void PathMapTileChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.OpenMap();
            }
            catch
            { }

        }



        #endregion

        #region PathMatrix
        public static readonly DependencyProperty PathMatrixProperty = DependencyProperty.Register("PathMatrix", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathMatrixChanged)));

        public string PathMatrix
        {
            get { return (string)GetValue(PathMatrixProperty); }
            set { SetValue(PathMatrixProperty, value); }
        }

        private static void PathMatrixChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.OpenMatrix();
            }
            catch
            { }

        }

        #endregion

        #region PathImageJammer
        public static readonly DependencyProperty PathImageJammerProperty = DependencyProperty.Register("PathImageJammer", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImageJammerChanged)));

        public string PathImageJammer
        {
            get { return (string)GetValue(PathImageJammerProperty); }
            set { SetValue(PathImageJammerProperty, value); }
        }

        private static void PathImageJammerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.InitializeObjectStyles();
               
            }
            catch
            { }

        }



        #endregion

        #region PathImageMulticopter
        public static readonly DependencyProperty PathImageMulticopterProperty = DependencyProperty.Register("PathImageMulticopter", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImageMulticopterChanged)));

        public string PathImageMulticopter
        {
            get { return (string)GetValue(PathImageMulticopterProperty); }
            set { SetValue(PathImageMulticopterProperty, value); }
        }

        private static void PathImageMulticopterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.InitializeObjectStyles();
                
            }
            catch
            { }

        }

        #endregion

        #region PathImagePlane
        public static readonly DependencyProperty PathImagePlaneProperty = DependencyProperty.Register("PathImagePlane", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImagePlaneChanged)));

        public string PathImagePlane
        {
            get { return (string)GetValue(PathImagePlaneProperty); }
            set { SetValue(PathImagePlaneProperty, value); }
        }

        private static void PathImagePlaneChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.InitializeObjectStyles();

            }
            catch
            { }

        }

        #endregion

        #region PathImageGNSS
        public static readonly DependencyProperty PathImageGNSSProperty = DependencyProperty.Register("PathImageGNSS", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImageGNSSChanged)));

        public string PathImageGNSS
        {
            get { return (string)GetValue(PathImageGNSSProperty); }
            set { SetValue(PathImageGNSSProperty, value); }
        }

        private static void PathImageGNSSChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.InitializeObjectStyles();
            }
            catch
            { }

        }



        #endregion

        #region PathImageSpoofing
        public static readonly DependencyProperty PathImageSpoofingProperty = DependencyProperty.Register("PathImageSpoofing", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImageSpoofingChanged)));

        public string PathImageSpoofing
        {
            get { return (string)GetValue(PathImageSpoofingProperty); }
            set { SetValue(PathImageSpoofingProperty, value); }
        }

        private static void PathImageSpoofingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.InitializeObjectStyles();
            }
            catch
            { }

        }



        #endregion

        #region PathImageOrientation
        public static readonly DependencyProperty PathImageOrientationProperty = DependencyProperty.Register("PathImageOrientation", typeof(string), typeof(RstMapView),
                                                                       new PropertyMetadata((string)"", new PropertyChangedCallback(PathImageOrientationChanged)));

        public string PathImageOrientation
        {
            get { return (string)GetValue(PathImageOrientationProperty); }
            set { SetValue(PathImageOrientationProperty, value); }
        }

        private static void PathImageOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.UpdateOrientationImage();

            }
            catch
            { }

        }



        #endregion

        #region AngleOrientation
        public static readonly DependencyProperty AngleOrientationProperty = DependencyProperty.Register("AngleOrientation", typeof(float), typeof(RstMapView),
                                                                       new PropertyMetadata((float)0, new PropertyChangedCallback(AngleOrientationChanged)));

        public float AngleOrientation
        {
            get { return (float)GetValue(AngleOrientationProperty); }
            set { SetValue(AngleOrientationProperty, value); }
        }

        private static void AngleOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.UpdateOrientationAngle();
            }
            catch
            { }

        }



        #endregion




        #region Projection

        public static readonly DependencyProperty ProjectionProperty = DependencyProperty.Register("Projection", typeof(MapProjection), typeof(RstMapView),
                                                                       new PropertyMetadata(new PropertyChangedCallback(ProjectionChanged)));
        public MapProjection Projection
        {
            get { return (MapProjection)GetValue(ProjectionProperty); }
            set { SetValue(ProjectionProperty, value); }
        }

        private static void ProjectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.mapControl.projection = (RSTMapProjection)rstMapView.Projection;
            }
            catch
            { }

        }

        #endregion


        #region ZoneAttention
        public static readonly DependencyProperty ZoneAttentionProperty = DependencyProperty.Register("ZoneAttention", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)5000, new PropertyChangedCallback(ZoneAttentionChanged)));

        
        
        public int ZoneAttention
        {
            get { return (int)GetValue(ZoneAttentionProperty); }
            set { SetValue(ZoneAttentionProperty, value); }
        }

        private static void ZoneAttentionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                    jammer.ZoneAttention.Radius = rstMapView.ZoneAttention;
            }
            catch
            { }

        }

        #endregion

        #region ZoneReadiness
        public static readonly DependencyProperty ZoneReadinessProperty = DependencyProperty.Register("ZoneReadiness", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)5000, new PropertyChangedCallback(ZoneReadinessChanged)));


        
        public int ZoneReadiness
        {
            get { return (int)GetValue(ZoneReadinessProperty); }
            set { SetValue(ZoneReadinessProperty, value); }
        }

        private static void ZoneReadinessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                    jammer.ZoneReadiness.Radius = rstMapView.ZoneReadiness;
            }
            catch { }
        }
        #endregion

        #region ZoneAlarm
        public static readonly DependencyProperty ZoneAlarmProperty = DependencyProperty.Register("ZoneAlarm", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)3000, new PropertyChangedCallback(ZoneAlarmChanged)));

        public int ZoneAlarm
        {
            get { return (int)GetValue(ZoneAlarmProperty); }
            set { SetValue(ZoneAlarmProperty, value); }
        }

        private static void ZoneAlarmChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneAlarm.Radius = rstMapView.ZoneAlarm;
                   
                }
            }
            catch { }

        }

        #endregion



        #region ZoneBearing
        public static readonly DependencyProperty ZoneBearingProperty = DependencyProperty.Register("ZoneBearing", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)3000, new PropertyChangedCallback(ZoneBearingChanged)));

        public int ZoneBearing
        {
            get { return (int)GetValue(ZoneBearingProperty); }
            set { SetValue(ZoneBearingProperty, value); }
        }

        private static void ZoneBearingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                    jammer.ZoneBearing.Radius = rstMapView.ZoneBearing;


            }
            catch { }

        }

        #endregion

        #region ZoneJamming
        public static readonly DependencyProperty ZoneJammingProperty = DependencyProperty.Register("ZoneJamming", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)3000, new PropertyChangedCallback(ZoneJammingChanged)));

        public int ZoneJamming
        {
            get { return (int)GetValue(ZoneJammingProperty); }
            set { SetValue(ZoneJammingProperty, value); }
        }

        private static void ZoneJammingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneJamming.Radius = rstMapView.ZoneJamming;
                    jammer.ZoneRotate.Radius = rstMapView.ZoneJamming;
                }
            }
            catch { }

        }

        #endregion

        #region ZoneOptic
        public static readonly DependencyProperty ZoneOpticProperty = DependencyProperty.Register("ZoneOptic", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)7000, new PropertyChangedCallback(ZoneOpticChanged)));

        public int ZoneOptic
        {
            get { return (int)GetValue(ZoneOpticProperty); }
            set { SetValue(ZoneOpticProperty, value); }
        }

        private static void ZoneOpticChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneOptic.Radius = rstMapView.ZoneOptic;
                   
                }
            }
            catch { }

        }

        #endregion


        #region ZoneConnect
        public static readonly DependencyProperty ZoneConnectProperty = DependencyProperty.Register("ZoneConnect", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)10000, new PropertyChangedCallback(ZoneConnectChanged)));

        public int ZoneConnect
        {
            get { return (int)GetValue(ZoneConnectProperty); }
            set { SetValue(ZoneConnectProperty, value); }
        }

        private static void ZoneConnectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneConnect.Radius = rstMapView.ZoneConnect;

                }
            }
            catch { }

        }
        #endregion



        #region DistanceRI
        public static readonly DependencyProperty DistanceRIProperty = DependencyProperty.Register("DistanceRI", typeof(int), typeof(RstMapView),
                                                                       new PropertyMetadata((int)100000, new PropertyChangedCallback(DistanceRIChanged)));

        public int DistanceRI
        {
            get { return (int)GetValue(DistanceRIProperty); }
            set { SetValue(DistanceRIProperty, value); }
        }

        private static void DistanceRIChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {

                    foreach (var b in jammer.BearingSource)
                        b.Radius = rstMapView.DistanceRI;

                    jammer.RaiseBearingSourceCollectionChanged();
                }


            }
            catch { }

        }

        #endregion



        



        #region SectorBearing
        public static readonly DependencyProperty SectorBearingProperty = DependencyProperty.Register("SectorBearing", typeof(float), typeof(RstMapView),
                                                                       new PropertyMetadata((float)22.5f, new PropertyChangedCallback(SectorBearingChanged)));

        public float SectorBearing
        {
            get { return (float)GetValue(SectorBearingProperty); }
            set { SetValue(SectorBearingProperty, value); }
        }

        private static void SectorBearingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                    jammer.ZoneBearing.Sector = rstMapView.SectorBearing;
            }
            catch { }

        }

        #endregion

        #region SectorJamming
        public static readonly DependencyProperty SectorJammingProperty = DependencyProperty.Register("SectorJamming", typeof(float), typeof(RstMapView),
                                                                       new PropertyMetadata((float)60, new PropertyChangedCallback(SectorJammingChanged)));

        public float SectorJamming
        {
            get { return (float)GetValue(SectorJammingProperty); }
            set { SetValue(SectorJammingProperty, value); }
        }

        private static void SectorJammingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneJamming.Sector = rstMapView.SectorJamming;
                    jammer.ZoneRotate.Sector = rstMapView.SectorJamming;
                }
                    
            }
            catch { }

        }

        #endregion


        #region SectorConnect
        public static readonly DependencyProperty SectorConnectProperty = DependencyProperty.Register("SectorConnect", typeof(float), typeof(RstMapView),
                                                                       new PropertyMetadata((float)7, new PropertyChangedCallback(SectorConnectChanged)));

        public float SectorConnect
        {
            get { return (float)GetValue(SectorConnectProperty); }
            set { SetValue(SectorConnectProperty, value); }
        }

        private static void SectorConnectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                foreach (var jammer in rstMapView.Jammers)
                {
                    jammer.ZoneConnectLink.Sector = rstMapView.SectorConnect;
                    jammer.ZoneConnectPC.Sector = rstMapView.SectorConnect;
                }

            }
            catch { }

        }
        #endregion


        #region CenterCoord

        public static readonly DependencyProperty CenterCoordProperty = DependencyProperty.Register("CenterCoord", typeof(Location), typeof(RstMapView),
                                                                       new PropertyMetadata(new PropertyChangedCallback(CenterCoordChanged)));



        public Location CenterCoord
        {
            get { return (Location)GetValue(CenterCoordProperty); }
            set { SetValue(CenterCoordProperty, value); }
        }

        private static void CenterCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                if (rstMapView.CenterCoord.Latitude != -1 && rstMapView.CenterCoord.Longitude != -1)
                    rstMapView.CenterCoordMap(rstMapView.CenterCoord);
            }
            catch { }

        }


        #endregion

        #region SpoofingPosition

        public static readonly DependencyProperty SpoofingPositionProperty = DependencyProperty.Register("SpoofingPosition", typeof(Location), typeof(RstMapView),
                                                                       new PropertyMetadata(new PropertyChangedCallback(SpoofingPositionChanged)));



        public Location SpoofingPosition
        {
            get { return (Location)GetValue(SpoofingPositionProperty); }
            set { SetValue(SpoofingPositionProperty, value); }
        }

        private static void SpoofingPositionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.UpdateSpoofing();

            }
            catch { }

        }


        #endregion

        #region GNSSPosition

        public static readonly DependencyProperty GNSSPositionProperty = DependencyProperty.Register("GNSSPosition", typeof(Location), typeof(RstMapView),
                                                                       new PropertyMetadata(new PropertyChangedCallback(GNSSPositionChanged)));



        public Location GNSSPosition
        {
            get { return (Location)GetValue(GNSSPositionProperty); }
            set { SetValue(GNSSPositionProperty, value); }
        }

        private static void GNSSPositionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.UpdateGNSS();
            }
            catch { }
        }

        #endregion

        #region OpenCloseMap

        public static readonly DependencyProperty OpenCloseMapProperty = DependencyProperty.Register("OpenCloseMap", typeof(Visibility),
                                                    typeof(RstMapView));

        public static readonly DependencyProperty OpenCloseMatrixProperty = DependencyProperty.Register("OpenCloseMatrix", typeof(Visibility),
                                                  typeof(RstMapView));


        public Visibility OpenCloseMap
        {
            get { return (Visibility)GetValue(OpenCloseMapProperty); }
            set { SetValue(OpenCloseMapProperty, value); }
        }

        public Visibility OpenCloseMatrix
        {
            get { return (Visibility)GetValue(OpenCloseMatrixProperty); }
            set { SetValue(OpenCloseMatrixProperty, value); }
        }


        #endregion

        #region Language

        public static readonly DependencyProperty CLanguageProperty = DependencyProperty.Register("CLanguage", typeof(MapLanguages), typeof(RstMapView),
                                                                       new PropertyMetadata(new PropertyChangedCallback(CLanguageChanged)));



        public MapLanguages CLanguage
        {
            get { return (MapLanguages)GetValue(CLanguageProperty); }
            set { SetValue(CLanguageProperty, value); }
        }

        private static void CLanguageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                rstMapView.SetResourceLanguage(rstMapView.CLanguage);
            }
            catch { }

        }

        #endregion

        

        #region ZorkiRTargetVisible

        public static readonly DependencyProperty ZorkiRTargetVisibleProperty = DependencyProperty.Register("ZorkiRTargetVisible", typeof(Dictionary<ViewZorkiR, bool>), typeof(RstMapView),
                                                                       new PropertyMetadata(new Dictionary<ViewZorkiR, bool>(), new PropertyChangedCallback(ZorkiRTargetVisibleChanged)));

        public Dictionary<ViewZorkiR, bool> ZorkiRTargetVisible
        {
            get { return (Dictionary<ViewZorkiR, bool>)GetValue(ZorkiRTargetVisibleProperty); }
            set { SetValue(ZorkiRTargetVisibleProperty, value); }
        }

        private static void ZorkiRTargetVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                rstMapView.UpdateZorkiRTargets();
            }
            catch
            { }
        }


        #endregion

        

        #region FormatViewCoord

        public static readonly DependencyProperty FormatViewCoordProperty = DependencyProperty.Register("FormatViewCoord", typeof(FormatCoord), typeof(RstMapView));



        public FormatCoord FormatViewCoord
        {
            get { return (FormatCoord)GetValue(FormatViewCoordProperty); }
            set { SetValue(FormatViewCoordProperty, value); }
        }



        #endregion


        #region BRDVisible

        public static readonly DependencyProperty BRDVisibleProperty = DependencyProperty.Register("BRDVisible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(BRDVisibleChanged)));



        public Visibility BRDVisible
        {
            get { return (Visibility)GetValue(BRDVisibleProperty); }
            set { SetValue(BRDVisibleProperty, value); }
        }

        private static void BRDVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                //rstMapView.VisibleZoneJamming = !Convert.ToBoolean(rstMapView.BRDVisible);


            }
            catch
            { }
        }


        #endregion

        #region OEMVisible

        public static readonly DependencyProperty OEMVisibleProperty = DependencyProperty.Register("OEMVisible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(OEMVisibleChanged)));



        public Visibility OEMVisible
        {
            get { return (Visibility)GetValue(OEMVisibleProperty); }
            set { SetValue(OEMVisibleProperty, value); }
        }

        private static void OEMVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                //rstMapView.VisibleZoneJamming = !Convert.ToBoolean(rstMapView.OEMVisible);


            }
            catch
            { }
        }


        #endregion


        #region AntennaTX1Visible

        public static readonly DependencyProperty AntennaTX1VisibleProperty = DependencyProperty.Register("AntennaTX1Visible", 
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(AntennaTX1VisibleChanged)));



        public Visibility AntennaTX1Visible
        {
            get { return (Visibility)GetValue(AntennaTX1VisibleProperty); }
            set { SetValue(AntennaTX1VisibleProperty, value); }
        }

        private static void AntennaTX1VisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
                
                rstMapView.VisibleZoneJamming = !Convert.ToBoolean(rstMapView.AntennaTX1Visible);
                

            }
            catch
            { }
        }


        #endregion


        #region AntennaTX2Visible

        public static readonly DependencyProperty AntennaTX2VisibleProperty = DependencyProperty.Register("AntennaTX2Visible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(AntennaTX2VisibleChanged)));



        public Visibility AntennaTX2Visible
        {
            get { return (Visibility)GetValue(AntennaTX2VisibleProperty); }
            set { SetValue(AntennaTX2VisibleProperty, value); }
        }

        private static void AntennaTX2VisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                //rstMapView.ZoneJammingSecondToggleButton.Visibility = rstMapView.AntennaTX2Visible;
                rstMapView.VisibleZoneJammingSecond = !Convert.ToBoolean(rstMapView.AntennaTX2Visible);


            }
            catch
            { }
        }


        #endregion

        #region AntennaLinkedVisible

        public static readonly DependencyProperty AntennaLinkedVisibleProperty = DependencyProperty.Register("AntennaLinkedVisible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(AntennaLinkedVisibleChanged)));



        public Visibility AntennaLinkedVisible
        {
            get { return (Visibility)GetValue(AntennaLinkedVisibleProperty); }
            set { SetValue(AntennaLinkedVisibleProperty, value); }
        }

        private static void AntennaLinkedVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                //rstMapView.ZoneConnectLinkToggleButton.Visibility = rstMapView.AntennaLinkedVisible;
                rstMapView.VisibleZoneConnectLink = !Convert.ToBoolean(rstMapView.AntennaLinkedVisible);


            }
            catch
            { }
        }


        #endregion


        #region AntennaPCVisible

        public static readonly DependencyProperty AntennaPCVisibleProperty = DependencyProperty.Register("AntennaPCVisible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(AntennaPCVisibleChanged)));



        public Visibility AntennaPCVisible
        {
            get { return (Visibility)GetValue(AntennaPCVisibleProperty); }
            set { SetValue(AntennaPCVisibleProperty, value); }
        }

        private static void AntennaPCVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                rstMapView.ZoneConnectPCToggleButton.Visibility = rstMapView.AntennaPCVisible;
                rstMapView.VisibleZoneConnectPC = !Convert.ToBoolean(rstMapView.AntennaPCVisible);


            }
            catch
            { }
        }


        #endregion

        #region OEMVisible

        public static readonly DependencyProperty OpticVisibleProperty = DependencyProperty.Register("OpticVisible",
                                                                    typeof(Visibility), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(OpticVisibleChanged)));



        public Visibility OpticVisible
        {
            get { return (Visibility)GetValue(OpticVisibleProperty); }
            set { SetValue(OpticVisibleProperty, value); }
        }

        private static void OpticVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {

                rstMapView.ZoneOpticToggleButton.Visibility = rstMapView.OpticVisible;
                rstMapView.VisibleZoneOptic = !Convert.ToBoolean(rstMapView.OpticVisible);


            }
            catch
            { }
        }


        #endregion


        #region TestMenuItem

        public static readonly DependencyProperty TestMenuItemProperty = DependencyProperty.Register("TestMenuItem",
                                                                    typeof(bool), typeof(RstMapView),
                                                                    new PropertyMetadata(new PropertyChangedCallback(TestMenuItemChanged)));



        public bool TestMenuItem
        {
            get { return (bool)GetValue(TestMenuItemProperty); }
            set { SetValue(TestMenuItemProperty, value); }
        }

        private static void TestMenuItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RstMapView rstMapView = (RstMapView)d;

            try
            {
               
            }
            catch
            { }
        }


        #endregion
    }
}