﻿public enum ViewUAV : byte
{
    Unknown = 0,
    Multicopter = 1,
    Plane = 2,
    GSM = 3
}