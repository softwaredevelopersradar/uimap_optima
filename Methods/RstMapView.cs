﻿
using Bearing;
using GrozaSModelsDBLib;
using Mapsui.Projection;
using Mapsui.Providers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using UIMap.Events;
using UIMap.Models;
using WpfMapControl;


namespace UIMap
{
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {
       
        public event EventHandler<Location> OnJammerPosition;
        public event EventHandler<float> OnDirection;
        public event EventHandler<string> OnMapOpen;

        public event EventHandler<float> OnDirectionJamming;
        public event EventHandler<float> OnDirectionJammingSecond;
        public event EventHandler<float> OnDirectionConnectLink;
        public event EventHandler<float> OnDirectionConnectPC;



        private List<VJammer> vJammerObject = new List<VJammer>();
        private List<VDrone> vDroneObject = new List<VDrone>();
        private List<VDrone> vRodnikObject = new List<VDrone>();
        private List<VDrone> vZorkiRObject = new List<VDrone>();
        private List<VDrone> vCuirasseObject = new List<VDrone>();


        private Feature _featureRout;

        private IMapObject _objectSpoofing;
        private IMapObject _objectGNSS;
        private IMapObject _objectStartPosition;
        private IMapObject _objectStopPosition;



        private MapObjectStyle _spoofingObjectStyle;
        private MapObjectStyle _gnssObjectStyle;
        private MapObjectStyle _jammerObjectStyle;
        private MapObjectStyle _multicopterObjectStyle;
        private MapObjectStyle _unknownObjectStyle;
        private MapObjectStyle _gsmObjectStyle;
        private MapObjectStyle _planeObjectStyle;
        private MapObjectStyle _startPointObjectStyle;
        private MapObjectStyle _stopPointObjectStyle;

        private MapObjectStyle _ballisticRodnikObjectStyle;
        private MapObjectStyle _balloonRodnikObjectStyle;
        private MapObjectStyle _helicopterRodnikObjectStyle;
        private MapObjectStyle _planeRodnikObjectStyle;
        private MapObjectStyle _unknownRodnikObjectStyle;


        private MapObjectStyle _humanZorkiRObjectStyle;
        private MapObjectStyle _carZorkiRObjectStyle;
        private MapObjectStyle _helicopterZorkiRObjectStyle;
        private MapObjectStyle _groupZorkiRObjectStyle;
        private MapObjectStyle _UAVZorkiRObjectStyle;
        private MapObjectStyle _IFVZorkiRObjectStyle;
        private MapObjectStyle _tankZorkiRObjectStyle;
        private MapObjectStyle _unknownZorkiRObjectStyle;


        private MapObjectStyle _cuirasseObjectStyle;

        private void UpdateSpoofing()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectSpoofing);

                    if (SpoofingPosition.Latitude != -1 && SpoofingPosition.Longitude != -1 && VisibleSpoofingPosition)
                    _objectSpoofing = DrawObject(_spoofingObjectStyle, SpoofingPosition, "Spoof");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateStartPosition()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectStartPosition);

                    if (StartPointRout.Latitude != -1 && StartPointRout.Longitude != -1)
                        _objectStartPosition = DrawObject(_startPointObjectStyle, StartPointRout, "Start");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateStopPosition(float Distance)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectStopPosition);

                    if (StopPointRout.Latitude != -1 && StopPointRout.Longitude != -1)
                        _objectStopPosition = DrawObject(_stopPointObjectStyle, StopPointRout, Distance.ToString()+" m");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateGNSS()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectGNSS);

                    if (GNSSPosition.Latitude != -1 && GNSSPosition.Longitude != -1 && VisibleGNSSPosition)
                        _objectGNSS = DrawObject(_gnssObjectStyle, GNSSPosition, "GNSS");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

       



        private void UpdateJammers()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (vJammerObject != null)
                    {
                        foreach (var jammer in vJammerObject)
                        {

                            mapControl.RemoveObject(jammer.mapJammer);
                            mapControl.RemoveObject(jammer.mapZoneAlarm);
                            mapControl.RemoveObject(jammer.mapZoneAttention);
                            mapControl.RemoveObject(jammer.mapZoneBearing);
                            mapControl.RemoveObject(jammer.mapZoneReadiness);
                            mapControl.RemoveObject(jammer.mapZoneOptic);
                            mapControl.RemoveObject(jammer.mapZoneJamming);
                            mapControl.RemoveObject(jammer.mapZoneJammingSecond);
                            mapControl.RemoveObject(jammer.mapZoneConnectLink);
                            mapControl.RemoveObject(jammer.mapZoneConnectPC);

                            mapControl.RemoveObject(jammer.mapZoneRotateJamming);
                            mapControl.RemoveObject(jammer.mapZoneRotateJammingSecond);
                            mapControl.RemoveObject(jammer.mapZoneRotateLink);
                            mapControl.RemoveObject(jammer.mapZoneRotatePC);

                            foreach (var b in jammer.mapBearing)
                                mapControl.RemoveObject(b);
                        }

                        vJammerObject.Clear();
                    }

                    foreach (var jammer in Jammers)
                    {
                        jammer.OnUpdateBearing += Jammer_OnUpdateBearing;
                        vJammerObject.Add(new VJammer(jammer.Id));

                       // vJammerObject.Last().mapJammer = DrawObject(_jammerObjectStyle,
                            //                          new Location(jammer.Coordinates.Longitude, jammer.Coordinates.Latitude, jammer.Coordinates.Altitude),
                           //                                            jammer.Id.ToString());

                        jammer.OnUpdateZone += ZoneUpdate;





                        jammer.ZoneAlarm.Radius = ZoneAlarm;
                        jammer.ZoneAlarm.Sector = 360;
                        jammer.ZoneAlarm.Visible = VisibleZoneCircle;
                        jammer.ZoneAlarm.PenZone = PenZoneAlarm;
                        jammer.ZoneAlarm.BrushZone = BrushZoneAlarm;


                        
                        jammer.ZoneAttention.Radius = ZoneAttention;
                        jammer.ZoneAttention.Sector = 360;
                        jammer.ZoneAttention.Visible = VisibleZoneCircle;
                        jammer.ZoneAttention.PenZone = PenZoneAttention;
                        jammer.ZoneAttention.BrushZone = BrushZoneAttention;
                        

                       
                        jammer.ZoneReadiness.Radius = ZoneReadiness;
                        jammer.ZoneReadiness.Sector = 360;
                        jammer.ZoneReadiness.Visible = VisibleZoneCircle;
                        jammer.ZoneReadiness.PenZone = PenZoneReadiness;
                        jammer.ZoneReadiness.BrushZone = BrushZoneReadiness;
                        

                        
                        jammer.ZoneBearing.Radius = ZoneBearing;
                        jammer.ZoneBearing.Sector = SectorBearing;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneBearing.Visible = VisibleZoneBearing;
                        else
                            jammer.ZoneBearing.Visible = false;

                        jammer.ZoneBearing.PenZone = PenZoneBearing;
                        jammer.ZoneBearing.BrushZone = BrushZoneBearing;
                        

                        
                        jammer.ZoneJamming.Radius = ZoneJamming;
                        jammer.ZoneJamming.Sector = SectorJamming;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneJamming.Visible = VisibleZoneJamming;
                        else
                            jammer.ZoneJamming.Visible = false;
                        jammer.ZoneJamming.PenZone = PenZoneJamming;
                        jammer.ZoneJamming.BrushZone = BrushZoneJamming;



                        jammer.ZoneJammingSecond.Radius = ZoneConnect;
                        jammer.ZoneJammingSecond.Sector = SectorConnect;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneJammingSecond.Visible = VisibleZoneJammingSecond;
                        else
                            jammer.ZoneJammingSecond.Visible = false;
                        jammer.ZoneJammingSecond.PenZone = PenZoneJammingSecond;
                        jammer.ZoneJammingSecond.BrushZone = BrushZoneJammingSecond;



                        jammer.ZoneConnectLink.Radius = ZoneJamming;
                        jammer.ZoneConnectLink.Sector = SectorJamming;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneConnectLink.Visible = VisibleZoneConnectLink;
                        else
                            jammer.ZoneConnectLink.Visible = false;
                        jammer.ZoneConnectLink.PenZone = PenZoneConnectLink;
                        jammer.ZoneConnectLink.BrushZone = BrushZoneConnectLink;



                        jammer.ZoneConnectPC.Radius = ZoneConnect;
                        jammer.ZoneConnectPC.Sector = SectorConnect;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneConnectPC.Visible = VisibleZoneConnectPC;
                        else
                            jammer.ZoneConnectPC.Visible = false;
                        jammer.ZoneConnectPC.PenZone = PenZoneConnectPC;
                        jammer.ZoneConnectPC.BrushZone = BrushZoneConnectPC;



                        jammer.ZoneOptic.Radius = ZoneOptic;
                        jammer.ZoneOptic.Sector = 40;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneOptic.Visible = VisibleZoneOptic;
                        else
                            jammer.ZoneOptic.Visible = false;
                        jammer.ZoneOptic.PenZone = PenZoneOptic;
                        jammer.ZoneOptic.BrushZone = BrushZoneOptic;


                        jammer.ZoneRotate.Radius = ZoneJamming;
                        jammer.ZoneRotate.Sector = SectorJamming;
                        jammer.ZoneRotate.Visible = false;
                        jammer.ZoneRotate.PenZone = PenZoneRotateJamming;
                        jammer.ZoneRotate.BrushZone = BrushZoneRotateJamming;

                        jammer.ZoneRotateJamming.Radius = ZoneJamming;
                        jammer.ZoneRotateJamming.Sector = SectorJamming;
                        jammer.ZoneRotateJamming.Visible = false;
                        jammer.ZoneRotateJamming.PenZone = PenZoneRotateJamming;
                        jammer.ZoneRotateJamming.BrushZone = BrushZoneRotateJamming;

                        jammer.ZoneRotateJammingSecond.Radius = ZoneConnect;
                        jammer.ZoneRotateJammingSecond.Sector = SectorConnect;
                        jammer.ZoneRotateJammingSecond.Visible = false;
                        jammer.ZoneRotateJammingSecond.PenZone = PenZoneRotateJammingSecond;
                        jammer.ZoneRotateJammingSecond.BrushZone = BrushZoneRotateJammingSecond;

                        jammer.ZoneRotateLink.Radius = ZoneJamming;
                        jammer.ZoneRotateLink.Sector = SectorJamming;
                        jammer.ZoneRotateLink.Visible = false;
                        jammer.ZoneRotateLink.PenZone = PenZoneRotateLink;
                        jammer.ZoneRotateLink.BrushZone = BrushZoneRotateLink;

                        jammer.ZoneRotatePC.Radius = ZoneConnect;
                        jammer.ZoneRotatePC.Sector = SectorConnect;
                        jammer.ZoneRotatePC.Visible = false;
                        jammer.ZoneRotatePC.PenZone = PenZoneRotatePC;
                        jammer.ZoneRotatePC.BrushZone = BrushZoneRotatePC;


                        Jammer_OnUpdateBearing(jammer, new EventArgs());
                       
                        
                    }

                    

                }), DispatcherPriority.Background);

            }

            catch
            { }

        }




        private void ZoneUpdate(object sender, string e)
        {

            var jammer = (VMJammer)sender;

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    switch (e)
                    {
                        case "Alarm":

                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAlarm);

                            if (jammer.ZoneAlarm.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAlarm = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneAlarm.LocationZone, jammer.ZoneAlarm.BrushZone, jammer.ZoneAlarm.PenZone, 2);

                            
                            break;

                        case "Readiness":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneReadiness);

                            if (jammer.ZoneReadiness.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneReadiness = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneReadiness.LocationZone, jammer.ZoneReadiness.BrushZone, jammer.ZoneReadiness.PenZone, 2);
                            break;

                        case "Attention":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAttention);

                            if (jammer.ZoneAttention.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAttention = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneAttention.LocationZone, jammer.ZoneAttention.BrushZone, jammer.ZoneAttention.PenZone, 2);
                            break;

                        case "Bearing":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneBearing);

                            if (jammer.ZoneBearing.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneBearing = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneBearing.LocationZone, jammer.ZoneBearing.BrushZone, jammer.ZoneBearing.PenZone, 2);
                            break;

                        case "Jamming":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJamming);

                            if (jammer.ZoneJamming.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneJamming.LocationZone, jammer.ZoneJamming.BrushZone, jammer.ZoneJamming.PenZone, 2);
                            break;

                        case "JammingSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJammingSecond);

                            if (jammer.ZoneJammingSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJammingSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneJammingSecond.LocationZone, jammer.ZoneJammingSecond.BrushZone, jammer.ZoneJammingSecond.PenZone, 2);
                            break;

                        case "ConnectLink":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectLink);

                            if (jammer.ZoneConnectLink.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectLink = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneConnectLink.LocationZone, jammer.ZoneConnectLink.BrushZone, jammer.ZoneConnectLink.PenZone, 2);
                            break;

                        case "ConnectPC":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectPC);

                            if (jammer.ZoneConnectPC.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectPC = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneConnectPC.LocationZone, jammer.ZoneConnectPC.BrushZone, jammer.ZoneConnectPC.PenZone, 2);
                            break;

                        case "Optic":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneOptic);

                            if (jammer.ZoneOptic.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneOptic = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneOptic.LocationZone, jammer.ZoneOptic.BrushZone, jammer.ZoneOptic.PenZone, 2);
                            break;

                        //case "Rotate":
                        //    mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotate);

                        //    if (jammer.ZoneRotate.Visible)
                        //        vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotate = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotate.LocationZone, jammer.ZoneRotate.BrushZone, jammer.ZoneRotate.PenZone, 2);
                        //    break;

                        case "RotateJamming":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJamming);

                            if (jammer.ZoneRotateJamming.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateJamming.LocationZone, jammer.ZoneRotateJamming.BrushZone, jammer.ZoneRotateJamming.PenZone, 2);
                            break;

                        case "RotateJammingSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJammingSecond);

                            if (jammer.ZoneRotateJammingSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJammingSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateJammingSecond.LocationZone, jammer.ZoneRotateJammingSecond.BrushZone, jammer.ZoneRotateJammingSecond.PenZone, 2);
                            break;

                        case "RotateLink":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateLink);

                            if (jammer.ZoneRotateLink.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateLink = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateLink.LocationZone, jammer.ZoneRotateLink.BrushZone, jammer.ZoneRotateLink.PenZone, 2);
                            break;

                        case "RotatePC":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotatePC);

                            if (jammer.ZoneRotatePC.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotatePC = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotatePC.LocationZone, jammer.ZoneRotatePC.BrushZone, jammer.ZoneRotatePC.PenZone, 2);
                            break;


                    }


                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        

        private void Jammer_OnUpdateBearing(object sender, EventArgs e)
        {
            var jammer = (VMJammer)sender;
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    
                    foreach (var tt in vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.ToList())
                        mapControl.RemoveObject(tt);

                    vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.Clear();

                    foreach (var b in jammer.BearingSource)                    
                    {
                        if (b.Visible && b.Direction > -1 )
                         vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.Add( DrawFeaturePolygon(TypeFeature.Polygon, b.locationBearing, null, b.PenBearing, 3));
                    }


                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        

        private void UpdateDrones()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vDroneObject != null)
                    foreach (var d in vDroneObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vDroneObject = new List<VDrone>(Drones.Count);


                foreach (var drone in Drones)
                {

                    if (drone.Visible)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = drone.IDSource;

                        switch (drone.View)
                        {
                            case ViewUAV.Multicopter:
                                tempdrone.mapObject = DrawObject(_multicopterObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            case ViewUAV.Plane:
                                tempdrone.mapObject = DrawObject(_planeObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            case ViewUAV.GSM:
                                tempdrone.mapObject = DrawObject(_gsmObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownObjectStyle, drone.LocationDrone, drone.Name);
                                break;


                        }

                        tempdrone.mapTrack = (drone.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, drone.LocationTrack, null, drone.PenTrack, 2) : null;

                        vDroneObject.Add(tempdrone);


                    }
                        

                    
                }
            }), DispatcherPriority.Background);


            UpdateTransparentSource();
        }



        private void UpdateRodnikTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vRodnikObject != null)
                    foreach (var d in vRodnikObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vRodnikObject = new List<VDrone>(RodnikTargets.Count);


                foreach (var rodnikTargets in RodnikTargets)
                {

                    if (rodnikTargets.Visible)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = rodnikTargets.IDSource;

                        switch (rodnikTargets.View)
                        {
                            case ViewRodnik.Ballistic:
                                tempdrone.mapObject = DrawObject(_ballisticRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Balloon:
                                tempdrone.mapObject = DrawObject(_balloonRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Helicopter:
                                tempdrone.mapObject = DrawObject(_helicopterRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Plane:
                                tempdrone.mapObject = DrawObject(_planeRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Unknown:
                                tempdrone.mapObject = DrawObject(_unknownRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;


                        }

                        tempdrone.mapTrack = (rodnikTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, rodnikTargets.LocationTrack, null, rodnikTargets.PenTrack, 2) : null;

                        vRodnikObject.Add(tempdrone);

                    }



                }
            }), DispatcherPriority.Background);

        }


        


        private void UpdateZorkiRTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vZorkiRObject != null)
                    foreach (var d in vZorkiRObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vZorkiRObject = new List<VDrone>(ZorkiRTargets.Count);


                foreach (var zorkiRTargets in ZorkiRTargets)
                {


                    bool VisibleTarget(ViewZorkiR key)
                    {
                        bool res = false;
                        bool hasValue = ZorkiRTargetVisible.TryGetValue(key, out res);

                        if (!hasValue)
                            return false;
                        else
                            return res;

                    }

                    bool visibleTemp = (VisibleTarget(zorkiRTargets.View));

                   
                    if (visibleTemp)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = zorkiRTargets.IDSource;

                        switch (zorkiRTargets.View)
                        {
                            case ViewZorkiR.Car:
                                tempdrone.mapObject = DrawObject(_carZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Group:
                                tempdrone.mapObject = DrawObject(_groupZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Helicopter:
                                tempdrone.mapObject = DrawObject(_helicopterZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Human:
                                tempdrone.mapObject = DrawObject(_humanZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.IFV:
                                tempdrone.mapObject = DrawObject(_IFVZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Tank:
                                tempdrone.mapObject = DrawObject(_tankZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.UAV:
                                tempdrone.mapObject = DrawObject(_UAVZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Unknown:
                                tempdrone.mapObject = DrawObject(_unknownZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                        }

                        tempdrone.mapTrack = (zorkiRTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, zorkiRTargets.LocationTrack, null, zorkiRTargets.PenTrack, 2) : null;

                        vZorkiRObject.Add(tempdrone);
                    }



                }
            }), DispatcherPriority.Background);


        }


        private void UpdateCuirasseTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vCuirasseObject != null)
                    foreach (var d in vCuirasseObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vCuirasseObject = new List<VDrone>(CuirasseTargets.Count);


                foreach (var cuirasseTargets in CuirasseTargets)
                {
                    
                    if (VisibleCuirasseTarget)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = cuirasseTargets.ID;

                        tempdrone.mapObject = DrawObject(_cuirasseObjectStyle, cuirasseTargets.LocationDrone, "");

                        tempdrone.mapTrack = DrawFeaturePolygon(TypeFeature.Polyline, cuirasseTargets.LocationTrack, null, cuirasseTargets.PenTrack, 2);

                        vCuirasseObject.Add(tempdrone);
                    }

                }
            }), DispatcherPriority.Background);


        }


        private void InitializeObjectStyles()
        {
            
            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));

            try
            {
                Bitmap imgSpoofing = (Bitmap)rm.GetObject("Spoofing");

                _spoofingObjectStyle = mapControl.LoadObjectStyle(imgSpoofing,
                        scale: 0.2, objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try 
            {
                Bitmap imgGNSS = (Bitmap)rm.GetObject("GNSS");

                _gnssObjectStyle = mapControl.LoadObjectStyle(imgGNSS,
                        scale: 0.8,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }


            try
            {
                Bitmap imgJammer = (Bitmap)rm.GetObject("Jammer");

                _jammerObjectStyle = mapControl.LoadObjectStyle(imgJammer,
                        scale: 1.5,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgCopter = (Bitmap)rm.GetObject("Copter");

                _multicopterObjectStyle = mapControl.LoadObjectStyle(imgCopter,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgUnknown = (Bitmap)rm.GetObject("Unknown");

                _unknownObjectStyle = mapControl.LoadObjectStyle(imgUnknown,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                

                Bitmap imgPlane = (Bitmap)rm.GetObject("Plane");

                _planeObjectStyle = mapControl.LoadObjectStyle(imgPlane,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {


                Bitmap imgGSM = (Bitmap)rm.GetObject("GSM");

                _gsmObjectStyle = mapControl.LoadObjectStyle(imgGSM,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            

            try
            {
                Bitmap imgStartPoint = (Bitmap)rm.GetObject("StartPoint");

                _startPointObjectStyle = mapControl.LoadObjectStyle(imgStartPoint,
                    scale: 0.3,
                    objectOffset: new Mapsui.Styles.Offset(0, 10),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgStopPoint = (Bitmap)rm.GetObject("StopPoint");

                _stopPointObjectStyle = mapControl.LoadObjectStyle(imgStopPoint,
                    scale: 0.06,
                    objectOffset: new Mapsui.Styles.Offset(0, 10),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }


            
        }

        private void InitializeRodnikObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));

           

            #region Rodnik
            try
            {
                Bitmap imgBallisticRodnik = (Bitmap)rm.GetObject("BombRodnik");

                _ballisticRodnikObjectStyle = mapControl.LoadObjectStyle(imgBallisticRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgBalloonRodnik = (Bitmap)rm.GetObject("BalloonRodnik");

                _balloonRodnikObjectStyle = mapControl.LoadObjectStyle(imgBalloonRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgHelicopterRodnik = (Bitmap)rm.GetObject("HelicopterRodnik");

                _helicopterRodnikObjectStyle = mapControl.LoadObjectStyle(imgHelicopterRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgPlaneRodnik = (Bitmap)rm.GetObject("PlaneRodnik");

                _planeRodnikObjectStyle = mapControl.LoadObjectStyle(imgPlaneRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUnknownRodnik = (Bitmap)rm.GetObject("UnknownRodnik");

                _unknownRodnikObjectStyle = mapControl.LoadObjectStyle(imgUnknownRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            #endregion
        }

        private void InitializeZorkiRObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region ZorkiR
            try
            {
                Bitmap imgHumanZorkiR = (Bitmap)rm.GetObject("HumanZorkiR");

                _humanZorkiRObjectStyle = mapControl.LoadObjectStyle(imgHumanZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgCarZorkiR = (Bitmap)rm.GetObject("CarZorkiR");

                _carZorkiRObjectStyle = mapControl.LoadObjectStyle(imgCarZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgHelicopterZorkiR = (Bitmap)rm.GetObject("HelicopterZorkiR");

                _helicopterZorkiRObjectStyle = mapControl.LoadObjectStyle(imgHelicopterZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgGroupZorkiR = (Bitmap)rm.GetObject("GroupZorkiR");

                _groupZorkiRObjectStyle = mapControl.LoadObjectStyle(imgGroupZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUAVZorkiR = (Bitmap)rm.GetObject("UAVZorkiR");

                _UAVZorkiRObjectStyle = mapControl.LoadObjectStyle(imgUAVZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgTankZorkiR = (Bitmap)rm.GetObject("TankZorkiR");

                _tankZorkiRObjectStyle = mapControl.LoadObjectStyle(imgTankZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgIFVZorkiR = (Bitmap)rm.GetObject("BMPZorkiR");

                _IFVZorkiRObjectStyle = mapControl.LoadObjectStyle(imgIFVZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUnknownZorkiR = (Bitmap)rm.GetObject("UnknownZorkiR");

                _unknownZorkiRObjectStyle = mapControl.LoadObjectStyle(imgUnknownZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            #endregion
        }



        private void InitializeCuirasseObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region Cuirasse
            try
            {
                Bitmap imgDroneCuirasse = (Bitmap)rm.GetObject("UAVZorkiR");

                _cuirasseObjectStyle = mapControl.LoadObjectStyle(imgDroneCuirasse,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

           

            #endregion
        }

        public async void OpenMap()
        {
            try
            {
                if (PathMapTile != "" && PathMap != "")
                {
                    mapControl.DataFolder = PathMapTile;
                    await mapControl.OpenMap(PathMap);
                    SliderZoom.Maximum = mapControl.MaxResolution;
                    SliderZoom.Minimum = mapControl.MinResolution;
                    OnMapOpen(mapControl, PathMap);
                }
            }
            catch 
            {
                
            }


        }

        public void CloseMap()
        {
            mapControl.CloseMap();

        }


        public void OpenMatrix()
        {
            try
            {
                if (PathMatrix != "")
                {
                    string[] files = Directory.GetFiles(PathMatrix);
                    mapControl.OpenDted(files);
                    
                }


            }
            catch
            {

            }


        }
        private void CenterMapGNSS()
        {
            try
            {
                CenterCoordMap(GNSSPosition);              
            }
            catch { }
        }

        private void CenterCoordMap(Location location)
        {
            try
            {
                
                mapControl.NavigateTo(ConvertToPoint( location));
            }
            catch { }
        }

        private void UpdateOrientationImage()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ImageDirection.PathImage = PathImageOrientation;

                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        private void UpdateOrientationAngle()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ImageDirection.Angle = AngleOrientation;

                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        private void UpdateTransparentSource()
        {
            Zone1TDF = CountZoneSource(1);
            Zone2TDF = CountZoneSource(2);
            Zone3TDF = CountZoneSource(3);

        }

        private byte CountZoneSource(byte zone)
        {
            byte _count =0;
            foreach (var drone in Drones)
                if (drone.Zone == zone)
                    _count++;

            return _count;
        }

        private void DrawRout()
        {

            Dispatcher.BeginInvoke(new Action(() =>
            {
                mapControl.RemoveObject(_featureRout);

                
            }), DispatcherPriority.Background);



            if (StartPointRout.Latitude != -1 && StartPointRout.Longitude != -1 &&
                StopPointRout.Latitude != -1 && StopPointRout.Longitude != -1 )
            {
                try
                {
                    List<Location> _locationRout = new List<Location>();
                    _locationRout.Add(new Location(StartPointRout.Latitude, StartPointRout.Longitude));
                    _locationRout.Add(new Location(StopPointRout.Latitude, StopPointRout.Longitude));

                    var dist = CalculateDistance(StartPointRout, StopPointRout);

                    UpdateStopPosition(dist);

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                       

                        _featureRout = DrawFeaturePolygon(TypeFeature.Polygon, _locationRout, null, PenRout, 3);
                    }), DispatcherPriority.Background);

                    
                    
                }
                catch
                { }
            }
        }

        private float CalculateDistance(Location Start, Location Finish)
        {
            float _distance = 0;
            if (Start.Latitude != -1 &&
                            Start.Longitude != -1 &&
                            Finish.Latitude != -1 &&
                            Finish.Longitude != -1)
                try
                {
                    
                       
                        
                        _distance = (float)Math.Round(ClassBearing.f_D_2Points(Start.Latitude,
                                Start.Longitude, Finish.Latitude, Finish.Longitude, 1), MidpointRounding.AwayFromZero);
                    
                }
                catch { }

            return _distance;
        }

    }
}
