﻿using System;
using System.Globalization;
using System.Windows.Data;
using CoordFormatLib;

namespace UIMap
{
    public class CoordConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
                        
            var coord = (double)value[0];

           
            if (coord == 0)
                return "  ";


            VCFormat VFcoord = new VCFormat(coord, 0);
          
            switch ((FormatCoord)value[1])
            {
                case FormatCoord.DD:
                    return VFcoord.coordDeg.Latitude.StringView;
                    //return coord.ToString("00.000000") + "\u00B0";

                case FormatCoord.DD_MM_mm:
                    return VFcoord.coordDegMin.Latitude.StringView;
                    //return VCFormat.ConvertToDM(coord);


                case FormatCoord.DD_MM_SS_ss:
                    return VFcoord.coordDegMinSec.Latitude.StringView;
                    //return VCFormat.ConvertToDMS(coord);

                default:
                    break;

            }
            return value;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
