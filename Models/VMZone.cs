﻿
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;
using GrozaSModelsDBLib;
using Mapsui.Projection;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Bearing;

namespace UIMap.Models

{
    public class VMZone:INotifyPropertyChanged
    {
        public event EventHandler<EventArgs> OnUpdate;

        

        public List<Location> LocationZone { get; private set; }


        
    

        private int _radius;

        public int Radius
        {
            get => _radius;
            set
            {
                if (_radius.Equals(value)) return;
                _radius = value;
                UpdatePolygon();
               
            }
        }

        private Coord _center = new Coord();

        public Coord Center
        {
            get => _center;
            set
            {
                if (_center.Equals(value)) return;
                _center = value;
                UpdatePolygon();
               
            }
        }


        private float _sector;

        public float Sector
        {
            get => _sector;
            set
            {
                if (_sector.Equals(value)) return;
                _sector = value;
                UpdatePolygon();
              
            }
        }


        private float _direction;

        public float Direction
        {
            get => _direction;
            set
            {
                if (_direction.Equals(value)) return;
                _direction = value;
                UpdatePolygon();
               

            }
        }

        private bool _visible ;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                UpdatePolygon();
                
            }
        }

        private string _name = "";

        public string Name
        {
            get => _name;
            set
            {
                if (_name.Equals(value)) return;
                _name = value;
                

            }
        }


        public Mapsui.Styles.Pen PenZone { get; set; }
        public Mapsui.Styles.Color BrushZone { get; set; }

        public VMZone(string name, Coord center)
        {
            Center = center;
            Name = name;
        }


        public VMZone(string name, Coord center, short radius, float sector, float direction, Mapsui.Styles.Pen penZone, Mapsui.Styles.Color brushZone)
        {
            _center = center;
            _radius = radius;
            _sector = sector;
            Name = name;
            PenZone = penZone;
            BrushZone = brushZone;
            Direction = direction;
        }


        public void UpdatePolygon()
        {
            try
            {
                float step = 1;

                int countPoint = 360;

                float fSectorLeft = (float)(Direction - Sector / 2);

                fSectorLeft = (fSectorLeft < 0) ? (360 - Math.Abs(fSectorLeft)) : fSectorLeft;


                float fSectorRight = (float)(Direction + Sector / 2);

                fSectorRight = (fSectorRight > 360) ? (fSectorRight - 360) : fSectorRight;


                countPoint = (fSectorRight - fSectorLeft > 0) ? (int)((fSectorRight - fSectorLeft) / step) : (int)((fSectorRight + 360 - fSectorLeft) / step);
                countPoint++;

                LocationZone = new List<Location>(countPoint + 2);

                if (Sector != 360)
                    LocationZone.Add(DefinePointSquare(Center, fSectorLeft, 0));
                else
                    LocationZone.Add(DefinePointSquare(Center, fSectorLeft, Radius));

                for (int i = 0; i < countPoint; i++)
                    LocationZone.Add(DefinePointSquare(Center, fSectorLeft + i * step, Radius));


                if (Sector != 360)
                    LocationZone.Add(DefinePointSquare(Center, fSectorLeft, 0));
                else
                    LocationZone.Add(DefinePointSquare(Center, fSectorLeft, Radius));

               

                OnUpdate?.Invoke(this, new EventArgs());
                


            }
            catch
            { }

            


        }

        private Location DefinePointSquare(Coord pointCenter, double Azimuth, double Distance)
        {
            Location location = new Location();

            try
            {
               
                double dLat = 0;
                double dLong = 0;



                ClassBearing.f_Bearing(Azimuth, Distance, 1, pointCenter.Latitude, pointCenter.Longitude, 1, ref dLat, ref dLong);

                location = new Location(dLat,dLong);
            }

            catch(Exception ex)
            {

            }
            return location;
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
