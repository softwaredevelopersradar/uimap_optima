﻿using System.Windows.Controls;

namespace UIMap
{
    public partial class RstMapView : UserControl
    {
        Mapsui.Styles.Pen PenZoneOptic = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Green,1);
        Mapsui.Styles.Color BrushZoneOptic = Mapsui.Styles.Color.FromArgb(100, 0, 255, 0);

        Mapsui.Styles.Pen PenZoneJamming = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Red, 1);
        Mapsui.Styles.Color BrushZoneJamming = Mapsui.Styles.Color.FromArgb(120, 255, 0, 0);

        Mapsui.Styles.Pen PenZoneJammingSecond = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Violet, 1);
        Mapsui.Styles.Color BrushZoneJammingSecond = Mapsui.Styles.Color.FromArgb(120, 128, 0, 128);


        Mapsui.Styles.Pen PenZoneConnectLink = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Orange, 1);
        Mapsui.Styles.Color BrushZoneConnectLink = Mapsui.Styles.Color.FromArgb(120, 255, 165, 0);

        Mapsui.Styles.Pen PenZoneConnectPC = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Yellow, 1);
        Mapsui.Styles.Color BrushZoneConnectPC = Mapsui.Styles.Color.FromArgb(120, 255, 255, 0);

        Mapsui.Styles.Pen PenZoneBearing = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Blue, 1);
        Mapsui.Styles.Color BrushZoneBearing = Mapsui.Styles.Color.FromArgb(100, 0, 0, 255);

        Mapsui.Styles.Pen PenBearing = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
        Mapsui.Styles.Pen PenSelectBearing = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Red, 1);

        Mapsui.Styles.Pen PenZoneAttention = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Green, 1);
        Mapsui.Styles.Color BrushZoneAttention = Mapsui.Styles.Color.FromArgb(50, 0, 255, 0);

        Mapsui.Styles.Pen PenZoneReadiness = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Blue, 1);
        Mapsui.Styles.Color BrushZoneReadiness = Mapsui.Styles.Color.FromArgb(50, 0, 0, 255);

        Mapsui.Styles.Pen PenZoneAlarm = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Red, 1);
        Mapsui.Styles.Color BrushZoneAlarm = Mapsui.Styles.Color.FromArgb(50, 255, 0, 0);

        Mapsui.Styles.Pen PenZoneRotate = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Yellow, 1);
        Mapsui.Styles.Color BrushZoneRotate = Mapsui.Styles.Color.FromArgb(50, 255, 179, 0);



        Mapsui.Styles.Pen PenZoneRotateJamming = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Red, 1);
        Mapsui.Styles.Color BrushZoneRotateJamming = Mapsui.Styles.Color.FromArgb(30, 255, 0, 0);

        Mapsui.Styles.Pen PenZoneRotateJammingSecond = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Violet, 1);
        Mapsui.Styles.Color BrushZoneRotateJammingSecond = Mapsui.Styles.Color.FromArgb(30, 128, 0, 128);

        Mapsui.Styles.Pen PenZoneRotateLink = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Orange, 1);
        Mapsui.Styles.Color BrushZoneRotateLink = Mapsui.Styles.Color.FromArgb(30, 255, 165, 0);

        Mapsui.Styles.Pen PenZoneRotatePC = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Yellow, 1);
        Mapsui.Styles.Color BrushZoneRotatePC = Mapsui.Styles.Color.FromArgb(30, 255, 255, 0);


        Mapsui.Styles.Pen PenRout = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Indigo, 1);

    }
}